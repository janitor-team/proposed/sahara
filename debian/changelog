sahara (1:18.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Mar 2023 14:43:44 +0100

sahara (1:18.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bookworm.
  * Switch to debhelper 11.
  * Removed Adapt_to_new_jsonschema_versions.patch applied upstream.
  * Removed --with systemd.
  * Removed lsb-base depends.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Mar 2023 08:46:19 +0100

sahara (1:17.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 06 Oct 2022 11:06:38 +0200

sahara (1:17.0.0~rc1-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Sep 2022 17:43:56 +0200

sahara (1:17.0.0~rc1-2) experimental; urgency=medium

  * Add missing --namespace oslo.middleware.healthcheck when generating
    sahara.conf.

 -- Thomas Goirand <zigo@debian.org>  Wed, 21 Sep 2022 14:40:36 +0200

sahara (1:17.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Sep 2022 10:16:55 +0200

sahara (1:16.0.0-2) unstable; urgency=medium

  * Get Ubuntu patches fixing compat with jsonschema 4.x (Closes: #1016222):
    - d/p/adapt-to-new-jsonschema-versions.patch: This fixes the majority of
      the new jsonschema unit test failures.
    - d/p/skip-test.patch: Skip the last jsonschema failure until it is fixed
      upstream.

 -- Thomas Goirand <zigo@debian.org>  Sat, 16 Jul 2022 11:07:28 +0200

sahara (1:16.0.0-1) unstable; urgency=medium

  * New usptream release.
  * Removed Fix_compatibility_with_oslo.context_4.0.0.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Mar 2022 22:36:16 +0200

sahara (1:16.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add Fix_compatibility_with_oslo.context_4.0.0.patch.

 -- Thomas Goirand <zigo@debian.org>  Mon, 28 Mar 2022 15:45:26 +0200

sahara (1:16.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Sat, 12 Mar 2022 20:51:43 +0100

sahara (1:15.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Oct 2021 21:31:41 +0200

sahara (1:15.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 20 Sep 2021 16:51:00 +0200

sahara (1:14.0.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 13:47:15 +0200

sahara (1:14.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Apr 2021 17:11:09 +0200

sahara (1:14.0.0~rc1-1) experimental; urgency=medium

  * Tune sahara-api-uwsgi.ini for performance.
  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bullseye.
  * Add remove-sphinxcontrib.httpdomain-from-sphinx-ext.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Mar 2021 14:16:48 +0100

sahara (1:13.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Sat, 17 Oct 2020 20:47:57 +0200

sahara (1:13.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Now using a yaml policy file in /etc/sahara/policy.d/.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Sep 2020 12:18:34 +0200

sahara (1:12.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 May 2020 19:20:16 +0200

sahara (1:12.0.0~rc2-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 09 May 2020 13:49:21 +0200

sahara (1:12.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Remove Fix_syntax_error_in_image_widths.patch.
  * Blacklist utils.test_api.APIUtilsTest.test_render_pagination().

 -- Thomas Goirand <zigo@debian.org>  Fri, 24 Apr 2020 16:45:24 +0200

sahara (1:11.0.0-4) unstable; urgency=medium

  * Add Fix_syntax_error_in_image_widths.patch (Closes: #955088).

 -- Thomas Goirand <zigo@debian.org>  Thu, 16 Apr 2020 12:40:41 +0200

sahara (1:11.0.0-3) unstable; urgency=medium

  * Blacklist 3 unit tests which are failing, and have been reported upstream
    at: https://storyboard.openstack.org/#!/story/2007450. (Closes: #954583).

 -- Thomas Goirand <zigo@debian.org>  Sun, 22 Mar 2020 16:37:47 +0100

sahara (1:11.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Oct 2019 22:32:33 +0200

sahara (1:11.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 16 Oct 2019 19:17:50 +0200

sahara (1:11.0.0~rc1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 01 Oct 2019 09:37:28 +0200

sahara (1:10.0.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Michal Arbet <michal.arbet@ultimum.io>  Wed, 17 Jul 2019 19:48:25 +0200

sahara (1:10.0.0-2) experimental; urgency=medium

  * d/control:
      - Bump openstack-pkg-tools to version 99
      - Add me to uploaders field
  * d/copyright: Add me to copyright file

 -- Michal Arbet <michal.arbet@ultimum.io>  Thu, 02 May 2019 19:06:21 +0200

sahara (1:10.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 11 Apr 2019 10:58:37 +0200

sahara (1:10.0.0~rc1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast

  [ Thomas Goirand ]
  * New upstream release.
  * Removed package versions when satisfied in Buster.
  * Add 2 missing new (build-)dependencies.
  * Do not use python3- prefix when calling config generators.
  * Removed fix-format-ssh-key-to-pem.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Sat, 30 Mar 2019 11:15:28 +0100

sahara (1:9.0.0-2) unstable; urgency=medium

  [ Michal Arbet ]
  * Add fix-format-ssh-key-to-pem.patch
  * Enable failing tests

 -- Michal Arbet <michal.arbet@ultimum.io>  Tue, 25 Sep 2018 14:52:43 +0200

sahara (1:9.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Remove /etc/sahara/policy.json on purge (Closes: #905506).
  * Uploading to unstable.
  * Blacklist 2 unit test due to SSL test failures:
    - test_crypto.CryptoTest.test_to_paramiko_private_key
    - test_crypto.CryptoTest.test_generate_key_pair

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Sep 2018 19:53:12 +0200

sahara (1:9.0.0~rc2-1) experimental; urgency=medium

  [ Thomas Goirand ]
  * Defines PYARGV in sahara-api.init.in to correctly load the config file.
  * Add fix-flask.request.content_length-is-none-in-sahara-api.patch.
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed fix-flask.request.content_length-is-none-in-sahara-api.patch
    applied upstream.

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 May 2018 09:14:15 +0200

sahara (1:8.0.1-1) unstable; urgency=medium

  * Switched sahara-api to uwsgi (b-d: openstack-pkg-tools >= 79~).
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 May 2018 21:54:14 +0200

sahara (1:8.0.0-2) unstable; urgency=medium

  * Fixed dbc postrm.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Mar 2018 18:00:00 +0000

sahara (1:8.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Switched to openstack-pkg-tools >= 70~ shared debconf templates.
  * Extended the python3-sahara lintian-overrides.
  * Standards-Version is now 4.1.3.

 -- Thomas Goirand <zigo@debian.org>  Wed, 07 Mar 2018 09:50:55 +0000

sahara (1:8.0.0~rc1-2) unstable; urgency=medium

  * Fixed debian/copyright holder list and years.

 -- Thomas Goirand <zigo@debian.org>  Wed, 28 Feb 2018 00:58:16 +0100

sahara (1:8.0.0~rc1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Switched to Python 3.

 -- Thomas Goirand <zigo@debian.org>  Tue, 27 Feb 2018 23:47:08 +0100

sahara (1:7.0.0-4) unstable; urgency=medium

  [ David Rabel ]
  * Add initial ru.po (Closes: #883463)

 -- Thomas Goirand <zigo@debian.org>  Fri, 26 Jan 2018 08:58:14 +0100

sahara (1:7.0.0-3) unstable; urgency=medium

  * Team upload.
  * Add e2fsprogs to Depends (Closes: #887246).

 -- Ondřej Nový <onovy@debian.org>  Wed, 17 Jan 2018 15:18:12 +0100

sahara (1:7.0.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Updated fr.po (Closes: #876312).

 -- Thomas Goirand <zigo@debian.org>  Sat, 28 Oct 2017 10:44:42 +0200

sahara (1:7.0.0-1) experimental; urgency=medium

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Correcting permissions in debian packaging files.
  * Updating standards version to 4.0.1.
  * Deprecating priority extra as per policy 4.0.1.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Fixed oslo-config-generator namespace list for this release.
  * Drop the 2 requirements.txt patches.
  * Remove b-d on dh-systemd.

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Oct 2017 00:41:40 +0200

sahara (1:5.0.0-3) unstable; urgency=medium

  * Team upload.
  * Bump build dependency on openstack-pkg-tools (Closes: #858711).

 -- David Rabel <david.rabel@noresoft.com>  Sat, 01 Apr 2017 12:11:59 +0200

sahara (1:5.0.0-2) unstable; urgency=medium

  * Team upload.
  * Patch-out upper constraints of SQLAlchemy

 -- Ondřej Nový <onovy@debian.org>  Fri, 20 Jan 2017 14:59:11 +0100

sahara (1:5.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 06 Oct 2016 18:21:53 +0200

sahara (1:5.0.0~rc1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/s/options: extend-diff-ignore of .gitreview
  * d/control: Use correct branch in Vcs-* fields

  [ Thomas Goirand ]
  * Uploading to unstable.
  * Build-Depends on openstack-pkg-tools >= 53~.
  * Fixed oslotest EPOCH.
  * Debconf translation:
    - it (Closes: #839527).

 -- Ondřej Nový <onovy@debian.org>  Mon, 26 Sep 2016 19:17:52 +0200

sahara (1:5.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using OpenStack's Gerrit as VCS URLs.
  * Adds debian/python-sahara.lintian-overrides because templates aren't fully
    scripts, and variables are fooling lintian.
  * Patches requirements.txt to allow build on Jessie.
  * Fixed missing EPOC in glanceclient (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Sep 2016 14:52:17 +0200

sahara (1:5.0.0~b2-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Added new pt_BR.po Brazilian Portuguese debconf templates translation
    (Closes: #829335).

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Jul 2016 10:49:42 +0200

sahara (1:5.0.0~b1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 13 Jun 2016 14:35:07 +0200

sahara (1:4.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/copyright: Changed source URL to https protocol

  [ Corey Bryant ]
  * d/rules: Install api-paste.ini.

  [ Thomas Goirand ]
  * Updated Portuguese translation for debconf messages (Closes: #821245).
  * Updated Dutch translation for debconf messages (Closes: #823258).

 -- Thomas Goirand <zigo@debian.org>  Fri, 22 Apr 2016 00:23:25 +0200

sahara (1:4.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 07 Apr 2016 21:58:07 +0200

sahara (1:4.0.0~rc2-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Added de.po debconf translation (Closes: #819689).

 -- Thomas Goirand <zigo@debian.org>  Tue, 05 Apr 2016 11:13:00 +0200

sahara (1:4.0.0~rc1-3) experimental; urgency=medium

  * Do not use Keystone admin auth token to register API endpoints.

 -- Thomas Goirand <zigo@debian.org>  Tue, 29 Mar 2016 13:53:38 +0000

sahara (1:4.0.0~rc1-2) experimental; urgency=medium

  * Add patch to install .json files as well.
  * Removed manual cp of alembic migration files, since the above patch makes
    it useless.

 -- Thomas Goirand <zigo@debian.org>  Tue, 29 Mar 2016 09:38:36 +0200

sahara (1:4.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Mar 2016 14:29:00 +0100

sahara (1:4.0.0~b3-3) experimental; urgency=medium

  * Added missing openssh-client and git build-depends-indep.
  * Also install alembic migration missed by PBR.

 -- Thomas Goirand <zigo@debian.org>  Tue, 15 Mar 2016 19:46:21 +0000

sahara (1:4.0.0~b3-2) experimental; urgency=medium

  * Better default config.

 -- Thomas Goirand <zigo@debian.org>  Tue, 08 Mar 2016 15:34:33 +0000

sahara (1:4.0.0~b3-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Fixed namespaces for generating the config file.
  * Using testr directly to run unit tests.
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Tue, 08 Mar 2016 14:06:54 +0100

sahara (1:4.0.0~b2-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed the use of "dpkg-parsechangelog -S" which doesn't work on Trusty.
  * Fixed debian/copyright ordering.

 -- Thomas Goirand <zigo@debian.org>  Thu, 10 Dec 2015 14:42:25 +0100

sahara (1:3.0.0-5) unstable; urgency=medium

  [ Mikhail Ivanov ]
  * d/rules: fix typo in rules with wrong package naming (ie: it was
    sahara-commom instead of common (see the m at the end instead of n)).

 -- Thomas Goirand <zigo@debian.org>  Tue, 03 Nov 2015 11:58:35 +0000

sahara (1:3.0.0-4) unstable; urgency=medium

  * Added q-text-as-data in depends of sahara-api.

 -- Thomas Goirand <zigo@debian.org>  Tue, 03 Nov 2015 11:58:35 +0000

sahara (1:3.0.0-3) unstable; urgency=medium

  * Rebuilt with openstack-pkg-tools 37 to use Keystone API v3.

 -- Thomas Goirand <zigo@debian.org>  Tue, 03 Nov 2015 09:14:17 +0000

sahara (1:3.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2015 20:05:35 +0000

sahara (1:3.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2015 17:05:17 +0200

sahara (1:3.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Added nl.po.
  * Now sahara-api depends on python-openstackclient.

 -- Thomas Goirand <zigo@debian.org>  Sat, 26 Sep 2015 20:02:03 +0000

sahara (1:3.0.0~b3-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Sep 2015 10:50:55 +0200

sahara (1:3.0.0~b2-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed debian/patches/also-install-alembic-migrations.patch applied
    upstream.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Aug 2015 16:30:57 +0200

sahara (2015.1.0-6) unstable; urgency=medium

  * Makes build reproducible thanks to Juan Picca <jumapico@gmail.com>
    (Closes: #788595).

 -- Thomas Goirand <zigo@debian.org>  Sun, 14 Jun 2015 15:49:50 +0200

sahara (2015.1.0-5) unstable; urgency=medium

  * Patches MANIFEST.in to get alembic_migrations in instead of copying
    them in debian/rules.

 -- Thomas Goirand <zigo@debian.org>  Fri, 12 Jun 2015 00:51:21 +0200

sahara (2015.1.0-4) unstable; urgency=medium

  * Added fr.po thanks to Julien Patriarca (Closes: #785354).

 -- Thomas Goirand <zigo@debian.org>  Sat, 30 May 2015 14:52:18 +0000

sahara (2015.1.0-3) unstable; urgency=medium

  * Removes obsolete patches.
  * Adds new sahara-api and sahara-engine packages, as this is now the
    recommended way to run Sahara. The old sahara package is now a
    metapackage (for compatibility reasons) which depends on them:
    - Moves files to sahara-common.install instead of sahara.install.
    - sahara-common Breaks+Replaces: the old sahara .deb.
    - Moves the sahara API endpoint registration to sahara-api.
    - Moves sahara.logrotate to sahara-common.logrotate, and makes it
      rotate /var/log/sahara/*.log.

 -- Thomas Goirand <zigo@debian.org>  Tue, 26 May 2015 13:51:04 +0200

sahara (2015.1.0-2) unstable; urgency=medium

  * Added pt.po translation of Debconf messages (Closes: #784595).

 -- Thomas Goirand <zigo@debian.org>  Tue, 12 May 2015 23:13:52 +0200

sahara (2015.1.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Apr 2015 22:02:01 +0000

sahara (2015.1~rc2-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Reviewed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 02 Jan 2015 17:35:30 +0100

sahara (2014.2.1-1) experimental; urgency=medium

  * New upstream release.
  * Now requires openstack-pkg-tools (>= 20~).

 -- Thomas Goirand <zigo@debian.org>  Mon, 15 Dec 2014 05:17:43 +0000

sahara (2014.2-2) experimental; urgency=medium

  * Fixed the sahara endpoint url from v1 to v1.1.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Oct 2014 20:55:18 +0800

sahara (2014.2-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 16 Oct 2014 20:10:10 +0800

sahara (2014.2~rc2-1) experimental; urgency=medium

  * New upstream release.
  * Review short description.

 -- Thomas Goirand <zigo@debian.org>  Sun, 12 Oct 2014 16:04:55 +0800

sahara (2014.2~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Added python-mysqldb as dependency.
  * Updated (build-)depends for this release.
  * Now using templated init script for sysv-rc, generated systemd unit and
    upstart jobs, using openstack-pkg-tools >= 13.
  * Removed Use_auth_uri_parameter_from_config.patch applied upstream.
  * Added doc-base registration file in sahara-doc.
  * Standards-Version is now 3.9.6 (no change).

 -- Thomas Goirand <zigo@debian.org>  Sun, 05 Oct 2014 11:16:07 +0800

sahara (2014.2~b3-1) experimental; urgency=medium

  * Initial release. (Closes: #762659)

 -- Thomas Goirand <zigo@debian.org>  Wed, 24 Sep 2014 16:34:46 +0800
