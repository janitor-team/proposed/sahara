Source: sahara
Section: net
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Michal Arbet <michal.arbet@ultimum.io>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools,
 po-debconf,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 alembic,
 bandit,
 openssh-client,
 python3-bashate,
 python3-botocore,
 python3-castellan,
 python3-cinderclient,
 python3-coverage,
 python3-doc8,
 python3-eventlet,
 python3-fixtures,
 python3-flask,
 python3-glanceclient,
 python3-hacking,
 python3-heatclient,
 python3-iso8601,
 python3-jinja2,
 python3-jsonschema,
 python3-keystoneauth1,
 python3-keystoneclient,
 python3-keystonemiddleware,
 python3-manilaclient,
 python3-microversion-parse,
 python3-migrate,
 python3-mysqldb,
 python3-neutronclient,
 python3-novaclient,
 python3-openstackdocstheme,
 python3-os-api-ref,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.context,
 python3-oslo.db,
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.messaging,
 python3-oslo.middleware,
 python3-oslo.policy,
 python3-oslo.rootwrap,
 python3-oslo.serialization,
 python3-oslo.service,
 python3-oslo.upgradecheck,
 python3-oslo.utils,
 python3-oslotest,
 python3-paramiko,
 python3-pep8,
 python3-psycopg2,
 python3-pymysql,
 python3-requests,
 python3-saharaclient,
 python3-sphinxcontrib.httpdomain,
 python3-sqlalchemy,
 python3-stestr,
 python3-stevedore,
 python3-swiftclient,
 python3-testresources,
 python3-testscenarios,
 python3-testtools,
 python3-tooz,
 python3-webob,
 subunit,
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/openstack-team/services/sahara
Vcs-Git: https://salsa.debian.org/openstack-team/services/sahara.git
Homepage: https://github.com/openstack/sahara

Package: python3-sahara
Architecture: all
Section: python
Depends:
 alembic,
 e2fsprogs,
 python3-botocore,
 python3-castellan,
 python3-cinderclient,
 python3-eventlet,
 python3-flask,
 python3-glanceclient,
 python3-heatclient,
 python3-iso8601,
 python3-jinja2,
 python3-jsonschema,
 python3-keystoneauth1,
 python3-keystoneclient,
 python3-keystonemiddleware,
 python3-manilaclient,
 python3-microversion-parse,
 python3-mysqldb,
 python3-neutronclient,
 python3-novaclient,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.context,
 python3-oslo.db,
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.messaging,
 python3-oslo.middleware,
 python3-oslo.policy,
 python3-oslo.rootwrap,
 python3-oslo.serialization,
 python3-oslo.service,
 python3-oslo.upgradecheck,
 python3-oslo.utils,
 python3-paramiko,
 python3-pbr,
 python3-psycopg2,
 python3-pymysql,
 python3-requests,
 python3-sqlalchemy,
 python3-stevedore,
 python3-swiftclient,
 python3-tooz,
 python3-webob,
 sqlite3,
 ${misc:Depends},
 ${python3:Depends},
Recommends:
 python3-ldap,
Description: OpenStack data processing cluster as a service - library
 The Sahara project provides a simple means to provision a data-intensive
 application cluster (Hadoop or Spark) on top of OpenStack. It's the ex
 Savanna project, renamed due to potential trademark issues.
 .
 This package contains the Python libraries.

Package: sahara
Architecture: all
Section: python
Depends:
 sahara-api (= ${source:Version}),
 sahara-engine (= ${source:Version}),
 ${misc:Depends},
 ${python3:Depends},
Description: OpenStack data processing cluster as a service - API & Engine
 The Sahara project provides a simple means to provision a data-intensive
 application cluster (Hadoop or Spark) on top of OpenStack. It's the ex
 Savanna project, renamed due to potential trademark issues.
 .
 This metapackage depends on the API server and the engine daemon.

Package: sahara-api
Architecture: all
Section: python
Depends:
 adduser,
 dbconfig-common,
 python3-openstackclient,
 python3-pastescript,
 q-text-as-data,
 sahara-common (= ${source:Version}),
 uwsgi-plugin-python3,
 ${misc:Depends},
 ${python3:Depends},
Description: OpenStack data processing cluster as a service - API server
 The Sahara project provides a simple means to provision a data-intensive
 application cluster (Hadoop or Spark) on top of OpenStack. It's the ex
 Savanna project, renamed due to potential trademark issues.
 .
 This package contains the API server.

Package: sahara-common
Architecture: all
Section: python
Depends:
 adduser,
 dbconfig-common,
 debconf,
 python3-sahara (= ${source:Version}),
 ${misc:Depends},
 ${python3:Depends},
Recommends:
 python3-ldap,
Description: OpenStack data processing cluster as a service - common files
 The Sahara project provides a simple means to provision a data-intensive
 application cluster (Hadoop or Spark) on top of OpenStack. It's the ex
 Savanna project, renamed due to potential trademark issues.
 .
 This package contains the common files.

Package: sahara-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: OpenStack data processing cluster as a service - doc
 The Sahara project provides a simple means to provision a data-intensive
 application cluster (Hadoop or Spark) on top of OpenStack. It's the ex
 Savanna project, renamed due to potential trademark issues.
 .
 This package contains the documentation.

Package: sahara-engine
Architecture: all
Section: python
Depends:
 adduser,
 dbconfig-common,
 sahara-common (= ${source:Version}),
 ${misc:Depends},
 ${python3:Depends},
Description: OpenStack data processing cluster as a service - Engine server
 The Sahara project provides a simple means to provision a data-intensive
 application cluster (Hadoop or Spark) on top of OpenStack. It's the ex
 Savanna project, renamed due to potential trademark issues.
 .
 This package contains the engine daemon.
